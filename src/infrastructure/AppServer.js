const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const routes = require('../application/routes');

class AppServer {
  constructor(port) {
    this.port = port;
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(cors());
    this.app.use(morgan('dev'));
  }

  addRoutes() {
    Object.values(routes).forEach((route) => {
      this.app.use(route);
    });
  }

  start() {
    this.addRoutes();

    this.app.listen(this.port, () => {
      console.log(`Server listening on port ${this.port}`);
    });
  }
}

module.exports = AppServer;