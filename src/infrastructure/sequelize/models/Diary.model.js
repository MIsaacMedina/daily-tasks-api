import { Model } from 'sequelize';

export default class Diary extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        text: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {},
        },
        dateId: {
          field: 'dateId',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Date is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'diaries',
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Date, {
      foreignKey: 'date_id',
      as: 'date',
    });
  }
}
