import { Model } from 'sequelize';

export default class User extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: 'Name is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'users',
      }
    );
  }

  static associate(models) {
    this.hasMany(models.Date, {
      foreignKey: 'user_id',
      as: 'dates',
    });
    this.belongsToMany(models.Company, {
      as: 'companies',
      through: 'companies_users',
      foreignKey: 'user_id'
    });
  }
}
