import { Model } from 'sequelize';

export default class CompanyRole extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        companyId: {
          field: 'company_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Company is mandatory' },
          },
        },
        roleId: {
          field: 'role_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Role is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'companies_roles',
      }
    );
  }

  static associate(models) { }
}
