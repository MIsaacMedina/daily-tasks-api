import { Model } from 'sequelize';

export default class CompanyUser extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        companyId: {
          field: 'company_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Company is mandatory' },
          },
        },
        userId: {
          field: 'user_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'User is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'companies_users',
      }
    );
  }

  static associate(models) { }
}
