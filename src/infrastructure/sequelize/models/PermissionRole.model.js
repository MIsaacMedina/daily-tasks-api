import { Model } from 'sequelize';

export default class PermissionRole extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        permissionId: {
          field: 'permission_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Permission is mandatory' },
          },
        },
        roleId: {
          field: 'role_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Role is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'permissions_roles',
      }
    );
  }

  static associate(models) { }
}
