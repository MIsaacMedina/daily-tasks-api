import { Model } from 'sequelize';

export default class Permission extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: 'Name is mandatory' },
          },
        },
        description: {
          type: Sequelize.STRING,
          allowNull: true,
          validate: {},
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'permissions',
      }
    );
  }

  static associate(models) {
    this.belongsToMany(models.Role, {
      as: 'roles',
      through: 'permissions_roles',
      foreignKey: 'permission_id',
    });
  }
}
