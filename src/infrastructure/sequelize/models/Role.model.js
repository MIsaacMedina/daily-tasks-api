import { Model } from 'sequelize';

export default class Role extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: 'Name is mandatory' },
          },
        },
        description: {
          type: Sequelize.STRING,
          allowNull: true,
          validate: {},
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'roles',
      }
    );
  }

  static associate(models) {
    this.belongsToMany(models.Permission, {
      as: 'permissions',
      through: 'permissions_roles',
      foreignKey: 'role_id'
    });
    this.belongsToMany(models.Company, {
      as: 'companies',
      through: 'companies_roles',
      foreignKey: 'role_id'
    });
  }
}
