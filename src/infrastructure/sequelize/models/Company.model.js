import { Model } from 'sequelize';

export default class User extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: 'Name is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'users',
      }
    );
  }

  static associate(models) {
    this.belongsToMany(models.User, {
      as: 'users',
      through: 'companies_users',
      foreignKey: 'company_id'
    });
    this.belongsToMany(models.Role, {
      as: 'roles',
      through: 'companies_roles',
      foreignKey: 'company_id'
    });
    this.hasMany(models.Task, {
      foreignKey: 'company_id',
      as: 'tasks',
    });
  }
}
