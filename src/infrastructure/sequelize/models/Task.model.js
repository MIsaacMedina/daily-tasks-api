import { Model } from 'sequelize';

export default class Task extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        companyId: {
          field: 'company_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Company is mandatory' },
          },
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: 'Name is mandatory' },
          },
        },
        description: {
          type: Sequelize.STRING,
          validate: {},
        },
        link: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {},
        },
        startDate: {
          field: 'start_date',
          type: Sequelize.DATE,
          allowNull: false,
          validate: {
            notNull: { msg: 'Start date is mandatory' },
          },
        },
        endDate: {
          field: 'endDate',
          type: Sequelize.DATE,
          allowNull: false,
          validate: {
            notNull: { msg: 'End date is mandatory' },
          },
        },
        estimation: {
          type: Sequelize.NUMBER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Estimation is mandatory' },
            max: 1000,
            min: 0,
          },
        },
        relatedTaskId: {
          field: 'related_task_id',
          type: Sequelize.INTEGER,
          validate: {},
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'tasks',
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Company, {
      foreignKey: 'company_id',
      as: 'company',
    });
    this.belongsTo(this, {
      foreignKey: 'related_task_id',
      as: 'related_task',
    });
    this.hasOne(this, {
      foreignKey: 'related_task_id',
      as: 'related_task',
    });
    this.belongsTo(models.Record, {
      foreignKey: 'task_id',
      as: 'task',
    });
  }
}
