import { Model } from 'sequelize';

export default class Record extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: 'Name is mandatory' },
          },
        },
        description: {
          type: Sequelize.STRING,
          validate: {},
        },
        dateId: {
          field: 'dateId',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'Date is mandatory' },
          },
        },
        startTime: {
          field: 'start_time',
          type: Sequelize.TIME,
          allowNull: false,
          validate: {
            notNull: { msg: 'Start time is mandatory' },
          },
        },
        endTime: {
          field: 'end_time',
          type: Sequelize.TIME,
          allowNull: false,
          validate: {
            notNull: { msg: 'End time is mandatory' },
          },
        },
        taskId: {
          field: 'task_id',
          type: Sequelize.INTEGER,
          validate: {},
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'records',
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Date, {
      foreignKey: 'date_id',
      as: 'date',
    });
    this.hasOne(models.Task, {
      foreignKey: 'task_id',
      as: 'task',
    });
  }
}
