import { Model } from 'sequelize';

export default class Date extends Model {
  static init(sequelize, Sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        date: {
          type: Sequelize.DATEONLY,
          allowNull: false,
          validate: {
            notNull: { msg: 'Date is mandatory' },
          },
        },
        userId: {
          field: 'user_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: 'User is mandatory' },
          },
        },
      },
      {
        sequelize,
        paranoid: true, // update deleted_at when destroy
        tableName: 'dates',
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user',
    });
    this.hasOne(models.Diary, {
      foreignKey: 'date_id',
      as: 'diary',
    });
    this.hasMany(models.Record, {
      foreignKey: 'date_id',
      as: 'records',
    });
  }
}
