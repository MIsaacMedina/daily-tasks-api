require('dotenv').config();
const AppServer = require('./src/infrastructure/AppServer');
const server = new AppServer(process.env.PORT || 3000);

server.start();
