# Defining base image
FROM node:latest

# Setting the working directory
WORKDIR /src

# Copying project files into the container
COPY package*.json .sequelizerc ./
COPY database.sql /docker-entrypoint-initdb.d/
COPY src/infrastructure/sequelize/config.js /usr/src/infrastructure/sequelize/config.js
COPY . .

# Installing dependencies
RUN npm i

# Exposing the MySQL port
EXPOSE ${DB_PORT}

# Exposing the NodeJS port
EXPOSE ${PORT}

# Charge .env and start the server
# CMD ["sh", "-c", "export $(grep -v '^#' .env | xargs) && npm run serve"]
# CMD [ "npm", "run", "serve" ]